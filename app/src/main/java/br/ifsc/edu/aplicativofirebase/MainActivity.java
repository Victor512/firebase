package br.ifsc.edu.aplicativofirebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    FirebaseAuth firebaseAuth;
    EditText editTextLogin, editTextSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextLogin = findViewById(R.id.editTextLogin);
        editTextSenha = findViewById(R.id.editTextSenha);

        firebaseAuth = FirebaseAuth.getInstance();

//        //Create new account
//        firebaseAuth.createUserWithEmailAndPassword("victor_pancracio@hotmail.com", "v1c70r");
//        firebaseAuth.signOut();
//
//        firebaseAuth.signInWithEmailAndPassword("victor_pancracio@hotmail.com", "v1c70r").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//            @Override
//            public void onComplete(@NonNull Task<AuthResult> task) {
//                if (task.isSuccessful()) {
//                    Toast.makeText(getApplicationContext(), firebaseAuth.getCurrentUser().getEmail(), Toast.LENGTH_LONG).show();
//                    Intent i = new Intent(getApplicationContext(), PrincipalActivity.class);
//                    startActivity(i);
//                } else {
//                    Toast.makeText(getApplicationContext(), "Falha no Login", Toast.LENGTH_LONG).show();
//                }
//            }
//        });
    }

    public void login(View view) {
        final String login = editTextLogin.getText().toString();
        String senha = editTextSenha.getText().toString();

        if(!login.trim().equals("")){
            firebaseAuth.signInWithEmailAndPassword(login,senha)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){
                        Toast.makeText(getApplicationContext(),"Sucesso! "+firebaseAuth.getCurrentUser().getEmail(), Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(getApplicationContext(),"Falha! "+login, Toast.LENGTH_LONG).show();
                    }
                }
            });

            FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
            if (firebaseUser != null){
                Intent intent = new Intent(getApplicationContext(),PrincipalActivity.class);
                startActivity(intent);
            }
        }
    }

    public void activityCadastrar(View view) {
        Intent i = new Intent(getApplicationContext(),CadastrarUsuarioActivity.class);
        startActivity(i);

    }

    public void recuperarSenha(View view) {
        if(!editTextLogin.getText().toString().trim().equals("")){
            firebaseAuth.sendPasswordResetEmail(editTextLogin.getText().toString());
        }
    }
}
